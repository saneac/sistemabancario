package com.ks.bancario.person;

import com.ks.bancario.accounts.Account;

import java.util.Hashtable;

/**
 * Created by migue on 17/11/2016.
 */
public class Client extends User
{
    private static long totalClientes;
    private static Hashtable<String, Client> clientHashtable;

    private Hashtable<String, Account> accountHashtable;
    private long idCliente;

    static
    {
        totalClientes = 0;
        clientHashtable = new Hashtable<String, Client>();
    }

    public Client()
    {
        totalClientes += 1;
        idCliente = totalClientes;
        accountHashtable = new Hashtable<String, Account>();
    }

    public void addAccount(Account account)
    {
        if (accountHashtable.containsKey(account.getCard()))
        {
            synchronized (accountHashtable)
            {
                accountHashtable.remove(account.getCard());
                accountHashtable.put(account.getCard(), account);
            }
        }
        else
        {
            synchronized (accountHashtable)
            {
                accountHashtable.put(account.getCard(), account);
                clientHashtable.put(account.getCard(), this);
            }
        }
    }

    public Account getAccount(String card)
    {
        if (accountHashtable.containsKey(card))
        {
            return accountHashtable.get(card);
        }
        else
        {
            return null;
        }
    }

    public double getIdCliente()
    {
        return idCliente;
    }

    public static long getTotalClientes()
    {
        return totalClientes;
    }

    public static Client getClient(String card)
    {
        if (clientHashtable.containsKey(card))
        {
            return  clientHashtable.get(card);
        }
        return null;
    }
}
